#!/usr/bin/env bash

######## TODO
#source ./paths.sh
export prefix="/vol/tiago/melodic-robocup/"
export PATH="${prefix}/bin:$PATH"

#ROS source alias
export setup_suffix=$(echo $SHELL | cut -d "/" -f3-)
alias source_ros="source ${prefix}/setup.${setup_suffix}"

#Map/World paths
export PATH_TO_MAPS="${prefix}/share/tiago_clf_nav/data"

#PocketSphinx paths
export PATH_TO_PSA_CONFIG="${prefix}/share/SpeechRec/psConfig"
##########

#Robot Setup
export basepc=$(hostname -s)
export laptop=${basepc}
export robot=${basepc}
export ROS_MASTER_URI=http://${basepc}:11311
export ROBOT_VERSION="steel"
export SIMMODE="true"

#Simulation map/world
export NAVIGATION_MAP="${PATH_TO_MAPS}/clf_furnished2-sim.yaml"
export SIMULATION_WORLD="furnished_clf2"

#Pocketsphinx_grammars
export VDEMO_PSA_CONFIG="/home/fthiemer/Robocup_WS/speechrec/psConfig/exercise/fthiemer.conf"

#Rviz config
export RVIZ_CONFIG="${prefix}/share/tobi_sim/config/tiago.rviz"

# Other
export ACTOR_VEL_TOPIC="/Olf/cmd_vel"
